import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Redirect
    // Link
} from 'react-router-dom'


import '../styles/_all-skins.css';
import '../styles/color-palette.css';
import '../styles/bootstrap.min.css';
import '../styles/AdminLTE.css';
import '../styles/App.css';


import Login from './components/Login';
import PageHeader from './components/PageHeader';
import PageFooter from './components/PageFooter';
// import Home from './components/Home';
import Sidebar from './components/Sidebar';
import ProjectList from './components/ProjectList';
import EmployeeList from './components/EmployeeList';

import moment from 'moment';
window.moment = moment;


class App extends Component {
    render() {
        return (
            <Router>
                <div className="wrapper skin-purple">
                    <Route exact path="/" component={Login}/>

                    <Route path="/home" component={PageHeader}/>
                    <Route path="/home" component={PageFooter}/>
                    <Route path="/home" component={Sidebar}/>
                    
                    <Route exact path="/home" render={() => <Redirect to="/home/projects"/>} />
                    <Route path="/home/employees" component={EmployeeList}/>
                    <Route path="/home/projects" component={ProjectList}/>
                    {/* <Route path="/home/projects" component={ProjectList}/> */}
                    
                    
                </div>
            </Router>
        );
    }
}

export default App;
