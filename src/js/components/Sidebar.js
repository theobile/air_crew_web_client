import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => {
    return (
        <aside className="main-sidebar">
            {/* <!-- sidebar: style can be found in sidebar.less --> */}
            <div className="slimScrollDiv" style={{position: 'relative', overflow: 'hidden', width: 'auto', height: '328px'}}>
                <section className="sidebar" style={{overflow: 'hidden', width: 'auto', 'height': '328px'}}>
                    {/* <!-- sidebar menu: : style can be found in sidebar.less --> */}
                    <ul className="sidebar-menu tree" data-widget="tree">
        {/* <li className="header">MAIN NAVIGATION</li> */}
        {/* <li className="treeview">
          <a href="#">
            <i className="fa fa-dashboard"></i> <span>Dashboard</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu">
            <li><a href="../../index.html"><i className="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i className="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li className="treeview active">
          <a href="#">
            <i className="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span className="pull-right-container">
              <span className="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul className="treeview-menu" style="display: none;">
            <li><a href="../layout/top-nav.html"><i className="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="../layout/boxed.html"><i className="fa fa-circle-o"></i> Boxed</a></li>
            <li className="active"><a href="../layout/fixed.html"><i className="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="collapsed-sidebar.html"><i className="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li>
          <a href="../widgets.html">
            <i className="fa fa-th"></i> <span>Widgets</span>
            <span className="pull-right-container">
              <small className="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li className="treeview">
          <a href="#">
            <i className="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu">
            <li><a href="../charts/chartjs.html"><i className="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="../charts/morris.html"><i className="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="../charts/flot.html"><i className="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="../charts/inline.html"><i className="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li className="treeview">
          <a href="#">
            <i className="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu">
            <li><a href="../UI/general.html"><i className="fa fa-circle-o"></i> General</a></li>
            <li><a href="../UI/icons.html"><i className="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="../UI/buttons.html"><i className="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="../UI/sliders.html"><i className="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="../UI/timeline.html"><i className="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="../UI/modals.html"><i className="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li className="treeview">
          <a href="#">
            <i className="fa fa-edit"></i> <span>Forms</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu">
            <li><a href="../forms/general.html"><i className="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="../forms/advanced.html"><i className="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="../forms/editors.html"><i className="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li className="treeview">
          <a href="#">
            <i className="fa fa-table"></i> <span>Tables</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu">
            <li><a href="../tables/simple.html"><i className="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="../tables/data.html"><i className="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="../calendar.html">
            <i className="fa fa-calendar"></i> <span>Calendar</span>
            <span className="pull-right-container">
              <small className="label pull-right bg-red">3</small>
              <small className="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="../mailbox/mailbox.html">
            <i className="fa fa-envelope"></i> <span>Mailbox</span>
            <span className="pull-right-container">
              <small className="label pull-right bg-yellow">12</small>
              <small className="label pull-right bg-green">16</small>
              <small className="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        <li className="treeview menu-open">
          <a href="#">
            <i className="fa fa-folder"></i> <span>Examples</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu" style="display: block;">
            <li><a href="../examples/invoice.html"><i className="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="../examples/profile.html"><i className="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="../examples/login.html"><i className="fa fa-circle-o"></i> Login</a></li>
            <li><a href="../examples/register.html"><i className="fa fa-circle-o"></i> Register</a></li>
            <li><a href="../examples/lockscreen.html"><i className="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="../examples/404.html"><i className="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="../examples/500.html"><i className="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="../examples/blank.html"><i className="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="../examples/pace.html"><i className="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>
        <li className="treeview"> 
          <a href="#">
            <i className="fa fa-share"></i> <span>Multilevel</span>
            <span className="pull-right-container">
              <i className="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul className="treeview-menu" style="display: none;">
            <li><a href="#"><i className="fa fa-circle-o"></i> Level One</a></li>
            <li className="treeview menu-open">
              <a href="#"><i className="fa fa-circle-o"></i> Level One
                <span className="pull-right-container">
                  <i className="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul className="treeview-menu" style="display: block;">
                <li><a href="#"><i className="fa fa-circle-o"></i> Level Two</a></li>
                <li className="treeview menu-open">
                  <a href="#"><i className="fa fa-circle-o"></i> Level Two
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul className="treeview-menu" style="display: block;">
                    <li><a href="#"><i className="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i className="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i className="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>*/}
                        <li className={`${document.location.pathname === '/home/projects'?'active': ''}`}>
                            <Link to="/home/projects">
                                <i className="fa fa-archive"></i> <span>Projects</span>
                            </Link>
                        </li>
                        <li className={`${document.location.pathname === '/home/employees'?'active': ''}`}>
                            <Link to="/home/employees">
                                <i className="fa fa-users"></i> <span>Employees</span>
                            </Link>
                        </li>
        {/* <li className="header">LABELS</li>
        <li><a href="#"><i className="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i className="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i className="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        <li className="bg-green"><a href="https://themequarry.com"><i className="fa fa-star-o" style="color: rgb(255, 255, 255);"></i><span style="color: rgb(255, 255, 255);">Premium Templates</span></a></li>*/}
                    </ul>
                </section>
                <div className="slimScrollBar" style={{background: 'rgb(0, 0, 0)', width: '7px', position: 'absolute', top: '-63.859px', opacity: '0.4', display: 'none', borderRadius: '7px', zIndex: 99, right: '1px', height: '91.1729px'}}></div>
                <div className="slimScrollRail" style={{width: '7px', height: '100%', position: 'absolute', top: '0px', display: 'none', borderRadius: '7px', background: 'rgb(51, 51, 51)', opacity: '0.2', zIndex: '90', right: '1px'}}></div>
            </div>
            {/* <!-- /.sidebar --> */}
        </aside>
    )
}

export default Sidebar;