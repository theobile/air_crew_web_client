import React, { Component } from 'react';
import { getRequestHeader } from '../utils';

class EmployeeList extends Component {
    constructor(props){
        super(props);
        this.state = {
            employees: []
        };
    }
    componentWillMount() {
        const that = this;
        fetch('/api/employees', {
            headers: getRequestHeader(),
            withCredentials: true
        }).then(function(obj){
            return obj.json();
        }).then(function(data){
            that.setState({
                employees: data
            })
        }).catch(err => {
            window.location.href = '/';
        })
    }
    render() {
        return (
            <div className="content-wrapper" style={{ 'minHeight': '327px'}}>
                <section className="content-header">
                <h1>
                    <i className="fa fa-users"> </i>  &nbsp;
                    Employees
                </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">All employees in the organisation</h3>
                                </div>
                                
                                <div className="box-body">
                                    <table className="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Age </th>
                                                <th> Nationality </th>
                                                <th> Phone </th>
                                                <th> Email </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.employees.map(emp => {
                                                return (
                                                    <tr key={emp.name}>
                                                        <td>{emp.name}</td>
                                                        <td>{emp.age}</td>
                                                        <td>{emp.nationality}</td>
                                                        <td>{emp.mobileNo}</td>
                                                        <td>{emp.email}</td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                                {this.state.employees.length === 0 &&
                                    <div className="overlay">
                                        <i className="fa fa-refresh fa-spin"></i>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>                   
                </section>
            </div>
        )
    }
}

export default EmployeeList;