import React from 'react';

const Modal = (props) => (
    <div className="modal fade in" id={props.modalId} data-backdrop="static">
        <div className="modal-dialog">
            <div className="modal-content">
                {props.children}
            </div>
        </div>
    </div>
);

const Header = (props) => (
    <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal">
            <i className="fa fa-times"></i>
        </button>
        <h4 className="modal-title">
        <i className="fa fa-archive"></i> &nbsp;
            {props.title}
        </h4>
        {props.children}
    </div>
);
const Body = (props) => (
    <div className="modal-body">
        {props.children}
    </div>
);

const Footer = (props) => (
    <div className="modal-footer">
        {props.hideCloseButton && 
            <div className="col-md-6">
                <button type="button" className="btn btn-default btn-lg" data-dismiss="modal">Cancel</button>
            </div>
        }
        
            {props.children}
        
        
    </div>
);

export { Modal, Header, Body, Footer };