import React from 'react';

const PageFooter = () => (
    <footer className="main-footer">
        <strong>Copyright © {new Date().getFullYear()}.</strong> All rights reserved.
    </footer>
)

export default PageFooter;