import React, { Component } from 'react';
import {Modal, Header, Body, Footer} from './Modal';
import AsyncSelect from 'react-select/lib/Async';


import { getRequestHeader } from '../utils';

const titles = {
    1: 'Step 1/2: Project Details',
    2: 'Step 2/2: Crew Details'
}

const initState = () => ({
    stepToRender: 1,
    projectCode: '',
    budget: '',
    startDate: new Date(),
    endDate: new Date(new Date().getTime() + 60*60*1000*24*6),
    crew: [],
    employeeSelectId: 'employee-select',
    key: 'selection'
});


const promiseOptions = inputValue => {
    return new Promise((resolve, reject) => {
        fetch(`/api/crewSearch?f=${inputValue}`, {
            headers: getRequestHeader(),
            withCredentials: true,

        }).then(function(obj){
            return obj.json();
        }).then(function(data){
            const crew = data.map(user => {
                return { id: user._id, label: user.name, value: user.email }
            })
            resolve(crew);
        }).catch(err => {
            console.log(err)
            reject(err);
        })
    });
}

class NewProjectModal extends Component {
    constructor(props) {
        super(props);
        this.state = initState();

        this.onTextChange = this.onTextChange.bind(this);
        this.handleNextClick = this.handleNextClick.bind(this);
        this.handleDateRangeChange = this.handleDateRangeChange.bind(this);
        this.handleAddCrew = this.handleAddCrew.bind(this);
        this.renderStep1 = this.renderStep1.bind(this);
        this.renderStep2 = this.renderStep2.bind(this);
        this.createProject = this.createProject.bind(this);
    }
    clearData() {
        const state = initState()
        this.setState(state);
    }
    onTextChange(e) {
        const { currentTarget: { name, value }} = e;
        this.setState({
            [name]: value
        })
    }
    handleNextClick(e) {
        e.preventDefault();
        this.setState({
            stepToRender: 2
        });
    }
    handleDateRangeChange(e) {
        const { currentTarget: { name, value }} = e;
        this.setState({
            [name]: new Date(value)
        });
    }
    handleAddCrew(crew) {
        this.setState({ crew: crew.map(c => c.id) });
    }
    renderStep1(){
        const { stepToRender, projectCode, budget, startDate, endDate } = this.state;
        return (
            <form className="form-horizontal" onSubmit={this.handleNextClick}>
                <Body>
                    <div className="box with-border">
                        <div className="box-header">
                            <h3 className="box-title ">{titles[stepToRender]}</h3>
                        </div>
                        <div className="box-body">

                                <div className="form-group">
                                    <label htmlFor="project-code" className="col-sm-3 control-label">Project Code</label>

                                    <div className="col-sm-9">
                                        <input 
                                            defaultValue={projectCode} 
                                            type="text" className="form-control input" 
                                            id="project-code"
                                            name="projectCode" 
                                            placeholder="PEK7 RAI2" 
                                            onChange={this.onTextChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="project-budget" className="col-sm-3 control-label">Budget</label>

                                    <div className="col-sm-9">
                                        <div className="input-group">
                                            <input 
                                                defaultValue={budget}
                                                type="number" 
                                                className="form-control input" 
                                                id="project-budget" 
                                                name="budget"
                                                placeholder="1000"
                                                onChange={this.onTextChange}
                                                required
                                            />
                                            <span className="input-group-addon">
                                                <i className="fa fa-euro"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="project-dates" className="col-sm-3 control-label">Start Date</label>

                                    <div className="col-sm-5">
                                        <input 
                                            name="startDate" 
                                            className="form-control input" 
                                            type="date" 
                                            onChange={this.handleDateRangeChange} 
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="project-dates" className="col-sm-3 control-label">End Date</label>

                                    <div className="col-sm-5">
                                        <input 
                                            name="endDate" 
                                            className="form-control input" 
                                            type="date" 
                                            onChange={this.handleDateRangeChange} 
                                            required
                                        />
                                    </div>
                                </div>
                        </div>
                    </div>
                    
                </Body>
                <Footer>
                    <button type="submit" className="btn btn-info btn-lg">Next</button>
                </Footer>
            </form>
        )
    }
    renderStep2(){
        const { crew } = this.state;
        return (
            <form className="form-horizontal">
                <Body>
                    <div className="box with-border">
                        <div className="box-header">
                            <h3 className="box-title "></h3>
                        </div>
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="crew-list" className="col-sm-2 control-label">Crew</label>
                                <div className="col-sm-10">
                                <AsyncSelect
                                    autoFocus
                                    isMulti
                                    isSearchable
                                    cacheOptions
                                    closeMenuOnSelectboolean={false}
                                    onChange={this.handleAddCrew}
                                    loadOptions={promiseOptions}  
                                />  
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </Body>
                <Footer>
                    <button className={"btn btn-info btn-lg " + (crew.length === 0? 'disabled':'')} onClick={this.createProject}>Done</button>
                </Footer>
            </form>
        )
    }
    createProject(e) {
        const that = this;

        e.preventDefault();
        const { projectCode, budget, startDate, endDate, crew } = this.state;
        fetch('/api/createProject', {
            method: 'POST',
            headers: getRequestHeader(),
            body: JSON.stringify({
                projectCode, budget, 
                startDate: startDate.getTime(), 
                endDate: endDate.getTime(), 
                crew
            }),
            mode: "cors",
        }).then(function(obj){
            return obj.json();
        }).then(function(data){
            that.props.onSuccess();
            window.jQuery(`#${that.props.modalId}`).modal('hide');
            
        }).catch(function(err) {
            console.log(err);
            alert('Error saving project. See console for error');
        })
    }
    render() {
        const { stepToRender } = this.state;
        return (
            <Modal modalId={this.props.modalId}>
                <Header title="Create a New Project" />
                {stepToRender === 1 && this.renderStep1()}
                {stepToRender === 2 && this.renderStep2()}
            </Modal>
        );
    }
}

export default NewProjectModal;