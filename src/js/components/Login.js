import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            inProgress: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        
    }
    handleSubmit(e) {
        e.preventDefault();
        const data = new FormData(e.target);

        fetch('/login', {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: data.get('email'),
              password: data.get('password')
            }),
            mode: "cors",
        }).then(function(obj){
            return obj.json();
        }).then(function(data){
          if (data.user && data.user._id){            
            localStorage.setItem('bearer', data.token);
            window.location.href = 'home';
          }
        })
    }

    render(){
        return (

            <div className="content-wrapper" style={{ 'minHeight': '327px', marginLeft: 0 }}>
                <section className="content">
                    <div className="row text-center">
                        <h1 className=""> <b>Air</b>Crew</h1>
                    </div>
                    <br />
                    <br />
                    <div className="row">
                        <div className="col-md-offset-4 col-md-4">
                            <div className="box box-info">
                                <div className="box-header with-border text-center">
                                    <h3 className="box-title "><b>Login </b></h3>
                                </div>

                                <div className="box-body">
                                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                                        <div className="box-body">
                                            <div className="form-group">
                                                {/* <label htmlFor="inputEmail3" className="col-sm-2 control-label">Email</label> */}

                                                <div className="col-sm-12">
                                                    <input defaultValue="test@test.com" name="email" type="email" className="form-control input-lg" id="inputEmail3" placeholder="Email" required />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                {/* <label htmlFor="inputPassword3" className="col-sm-2 control-label">Password</label> */}

                                                <div className="col-sm-12">
                                                    <input defaultValue="password" name="password" type="password" className="form-control input-lg" id="inputPassword3" placeholder="Password" required />
                                                </div>
                                            </div>
                                            {/* <div className="form-group">
                                                <div className="col-sm-offset-2 col-sm-10">
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" /> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                        {/* <!-- /.box-body --> */}
                                        <div className="box-footer text-center">
                                            <button type="submit" onClick={this.beginLogin} className="btn btn-lg btn-info ">Sign in</button>
                                        </div>
                                        {/* <!-- /.box-footer --> */}
                                    </form>
                                </div>
                    
                            </div>
                        </div>
                    </div>
                </section>
            
                {/* <!-- Horizontal Form --> */}
                
                {/* <!-- /.box --> */}
                {/* <!-- general form elements disabled --> */}
          <div className="box box-warning" style={{display: 'none'}}>
            <div className="box-header with-border">
              <h3 className="box-title">General Elements</h3>
            </div>
            {/* <!-- /.box-header --> */}
            <div className="box-body">
              <form>
                {/* <!-- text input --> */}
                <div className="form-group">
                  <label>Text</label>
                  <input type="text" className="form-control" placeholder="Enter ..." />
                </div>
                <div className="form-group">
                  <label>Text Disabled</label>
                  <input type="text" className="form-control" placeholder="Enter ..." disabled="" />
                </div>

                {/* <!-- textarea --> */}
                <div className="form-group">
                  <label>Textarea</label>
                  <textarea className="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
                <div className="form-group">
                  <label>Textarea Disabled</label>
                  <textarea className="form-control" rows="3" placeholder="Enter ..." disabled=""></textarea>
                </div>

                {/* <!-- input states --> */}
                <div className="form-group has-success">
                  <label className="control-label" htmlFor="inputSuccess"><i className="fa fa-check"></i> Input with success</label>
                  <input type="text" className="form-control" id="inputSuccess" placeholder="Enter ..." />
                  <span className="help-block">Help block with success</span>
                </div>
                <div className="form-group has-warning">
                  <label className="control-label" htmlFor="inputWarning"><i className="fa fa-bell-o"></i> Input with
                    warning</label>
                  <input type="text" className="form-control" id="inputWarning" placeholder="Enter ..." />
                  <span className="help-block">Help block with warning</span>
                </div>
                <div className="form-group has-error">
                  <label className="control-label" htmlFor="inputError"><i className="fa fa-times-circle-o"></i> Input with
                    error</label>
                  <input type="text" className="form-control" id="inputError" placeholder="Enter ..." />
                  <span className="help-block">Help block with error</span>
                </div>

                {/* <!-- checkbox --> */}
                <div className="form-group">
                  <div className="checkbox">
                    <label>
                      <input type="checkbox" />
                      Checkbox 1
                    </label>
                  </div>

                  <div className="checkbox">
                    <label>
                      <input type="checkbox" />
                      Checkbox 2
                    </label>
                  </div>

                  <div className="checkbox">
                    <label>
                      <input type="checkbox" disabled="" />
                      Checkbox disabled
                    </label>
                  </div>
                </div>

                {/* <!-- radio --> */}
                <div className="form-group">
                  <div className="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="" />
                      Option one is this and that—be sure to include why it's great
                    </label>
                  </div>
                  <div className="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" />
                      Option two can be something else and selecting it will deselect option one
                    </label>
                  </div>
                  <div className="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled="" />
                      Option three is disabled
                    </label>
                  </div>
                </div>

                {/* <!-- select --> */}
                <div className="form-group">
                  <label>Select</label>
                  <select className="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Select Disabled</label>
                  <select className="form-control" disabled="">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>

                {/* <!-- Select multiple--> */}
                <div className="form-group">
                  <label>Select Multiple</label>
                  <select multiple="" className="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Select Multiple Disabled</label>
                  <select multiple="" className="form-control" disabled="">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>

              </form>
            </div>
            {/* <!-- /.box-body --> */}
          </div>
          {/* <!-- /.box --> */}
        </div>
        )
    }
}

export default Login;