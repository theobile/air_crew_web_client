import React, { Component } from 'react';
import { HumanDate } from '../utils';
import NewProjectModal from './NewProjectModal';

class ProjectList extends Component {
    constructor(props){
        super(props);
        const initState = {
            projects: [],
            newProjectModalId: 'new-project-modal',
        };
        
        this.state = initState;
        this.toggleNewProjectModal = this.toggleNewProjectModal.bind(this);
        this.fetchProjects = this.fetchProjects.bind(this);
    }
    fetchProjects() {
        const that = this;
        fetch('/api/projects', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('bearer')}`,
                'Content-Type': 'application/json',
                'credentials': 'same-origin'
            },
            withCredentials: true
        }).then(function(obj){
            return obj.json();
        }).then(function(data){
            that.setState({
                projects: data
            });
        }).catch(err => {
            window.location.href = '/';
        })
    }
    componentWillMount() {
        this.fetchProjects();
    }
    componentDidMount(){
        this.modalRef = React.createRef();
        window.jQuery(`#${this.state.newProjectModalId}`).on('show.bs.modal',  () => {
            this.modalRef.current.clearData();
        });
    }
    toggleNewProjectModal(option, refetch) {
        if (option && option === 'show')
            window.jQuery(`#${this.state.newProjectModalId}`).modal('show');
        else {
            if (refetch) this.fetchProjects();
            window.jQuery(`#${this.state.newProjectModalId}`).modal('hide');
        }

    }
    render() {
        return (
            <div className="content-wrapper" style={{ 'minHeight': '327px'}}>
                <section className="content-header">
                    <h1>
                        <i className="fa fa-archive"> </i>  &nbsp;
                        Projects &nbsp;&nbsp;&nbsp;
                        <button className="btn btn-info pull-right" onClick={() => this.toggleNewProjectModal('show')}> 
                            <i className="fa fa-plus"> </i> &nbsp;
                            Create New Project 
                        </button>
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        {this.state.projects.map(project => (
                            <div className="col-md-4" key={project.startDate}>
                                <div className="box box-solid box-warning">
                                    <div className="box-header">
                                        <h3 className="box-title">
                                            {project.projectCode}
                                        </h3>
                                    </div>
                                    <div className="box-body">
                                        {HumanDate(project.startDate)} - {HumanDate(project.endDate)}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </section>
                <NewProjectModal onSuccess={() => {this.toggleNewProjectModal('hide', true)}} ref={this.modalRef} modalId={this.state.newProjectModalId} />
            </div>
        )
    }
}

export default ProjectList;