import React, { Component } from 'react';

class Home extends Component {
    componentWillMount() {
        fetch('/api/profile', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('bearer')}`,
                'Content-Type': 'application/x-www-form-urlencoded',
                'credentials': 'same-origin'
            },
            withCredentials: true
        }).then(function(obj){
            return obj.json();
        }).then(function(data){
            localStorage.setItem('profile', JSON.stringify(data));
        }).catch(err => {
            window.location.href = '/';
        });
    }

    render() {
        return (
            <div>
                Home page!
            </div>
        );
    }
}

export default Home;