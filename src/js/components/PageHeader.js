import React, { Component } from 'react';

class PageHeader extends Component {
    render() {
        
        return (
            <header className="main-header">
                <a href="/home" className="logo">
                {/* <!-- mini logo for sidebar mini 50x50 pixels --> */}
                    <span className="logo-mini"><b>A</b>C</span>
                    {/* <!-- logo for regular state and mobile devices --> */}
                    <span className="logo-lg"><b>Air</b>Crew</span>
                </a>
            <nav className="navbar navbar-static-top">
              <div className="container-fluid">
                <div className="navbar-header">
                  {/* <a href="/" className="navbar-brand"><b>AirCrew</b></a> */}
                  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i className="fa fa-bars"></i>
                  </button>
                </div>
        
                {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
                {/* <div className="collapse navbar-collapse" id="navbar-collapse">
                  <ul className="nav navbar-nav">
                    <li className="active"><a href="#">Link <span className="sr-only">(current)</span></a></li>
                    <li><a href="#">Link</a></li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown">Dropdown <span className="caret"></span></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li className="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                      </ul>
                    </li>
                  </ul>
                  <form className="navbar-form navbar-left" role="search">
                    <div className="form-group">
                      <input type="text" className="form-control" id="navbar-search-input" placeholder="Search" />
                    </div>
                  </form>
                  <ul className="nav navbar-nav navbar-right">
                    <li><a href="#">Link</a></li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown">Dropdown <span className="caret"></span></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li className="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </li>
                  </ul>
                </div> */}
                {/* <!-- /.navbar-collapse --> */}
              </div>
              {/* <!-- /.container-fluid --> */}
            </nav>
          </header>
        );
    }
}

export default PageHeader;