const moment = require('moment')

export function HumanDate(date) {
    return moment(date).format('Do MMM, YYYY')
}

export function HumanNumber(number) {
    try {
        const n = parseInt(number)
        return n.toLocaleString();
    }
    catch(e) {
        console.log(e);
    }
}

export function getRequestHeader(){
    return {
        'Authorization': `Bearer ${localStorage.getItem('bearer')}`,
        'Content-Type': 'application/json',
        'credentials': 'same-origin'
    };
}